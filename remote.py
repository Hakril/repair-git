import tempfile
import glob
import os.path
import shutil
from follow import launch_command

def get_remote_addr(remote_name):
    stdout, stderr = launch_command("git remote -v")
    stdout = stdout.decode()
    for remote_line in stdout.split("\n"):
        if remote_name in remote_line and "fetch" in remote_line:
            return remote_line.split()[1]
    raise ValueError("Unkown remote :" + remote_name)

def clone_bare_remote(git, remote_name):
    if hasattr(git, "clone"):
        return git.clone
    dirname = tempfile.mkdtemp()
    remote_addr = get_remote_addr(remote_name)
    print ("cloning {0} into {1}".format(remote_addr, dirname))
    stdout, stderr = launch_command("git clone {0} {1}".format(remote_addr, dirname))
    if stderr:
        raise ValueError("erreur during cloning : {0}".format(stderr))
    git.clone = dirname
    return dirname

def get_clone_pack(clonedir):
    return glob.glob(clonedir + "/.git/objects/pack/*.pack")


def object_path_from_sha1(sha1):
    return ".git/objects/{0}/{1}".format(sha1[0:2], sha1[2:])

def reload_missing_objects_from_remote(git, remote_name, missing_list):
    clonedir = clone_bare_remote(git, remote_name)
    #copy non packed
    for sha1 in missing_list:
        path = object_path_from_sha1(sha1)
        tmppath = clonedir + "/" + path
        if os.path.exists(tmppath):
            shutil.copyfile(tmppath, path)
    #extract pack
    packs = get_clone_pack(clonedir)
    for pack in packs:
        launch_command("git unpack-objects < {0}".format(pack))
    return

def remote_repair_broken_list(git, broken_list):
    for sha1 in broken_list:
        path = object_path_from_sha1(sha1)
        if os.path.exists(path):
            os.remove(path)
    reload_missing_objects_from_remote(git, 'origin', broken_list)

