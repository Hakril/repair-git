import os.path
import re

from follow import launch_command, follower
from remote import remote_repair_broken_list

git_subdir = ".git"
git_cat_file = "git cat-file -p "

class Sha1Error(ValueError):
    pass

class RemoteSha1Error(Sha1Error):
    def __init__(self, remote, name):
        self.remote = remote
        self.name = name

def read_all(path):
    return open(path).read()

def is_sha1(sha1):
    return re.match("[0-9a-z]{40}", sha1)

def read_sha1(f):
    data = read_all(f).strip()
    if not is_sha1(data):
        raise Sha1Error("Corrupted sha1 file <{0}>".format(f))
    return data

class GitRepo(dict):

    def branch_generator(self):
        for name, branch in self["refs"]["heads"].items():
            yield (name, read_all(branch)) 


    def _gen_remote(self, remote):
        for name, branch_path in self["refs"]["remotes"][remote].items():
                try:
                    yield (remote, name, read_sha1(branch_path))
                except Sha1Error:
                    # Corrupt
                    yield (remote, name, None)

    def remote_generator(self, remote=None):
        if "remotes" not in self["refs"]:
            return
        if remote is None:
            for remote in self["refs"]["remotes"]:
                for r in self._gen_remote(remote):
                    yield r
            return
        if remote not in self["refs"]["remotes"]:
             raise KeyError("remote <{0}> not found".format(remote))
        for rem in self._gen_remote(remote):
            yield rem


def check_root_git(path):
    if not os.path.exists(path + os.sep + git_subdir):
        print (".git not found in path <{0}>".format(path))
        exit (1)
    return

def generate_complete_git_arbo(path):
    git = GitRepo()
    path = path + os.sep + git_subdir
    for (dirpath, dirname, filename) in os.walk(path):
        # remove git_subdir prefix
        relapath = dirpath[len(path) + 1:]
        d = get_git_dir(git, relapath)
        for dir in dirname:
            d[dir] = {}
        for file in filename:
            d[file] = dirpath + os.sep + file
    return git

def get_git_dir(git, path):
    d = git
    path = os.path.normpath(path)
    if path == ".":
        return git
    for dirname in path.split(os.sep):
        if dirname != "":
            d = d[dirname]
    return d

def remote_repair(git, foll=None):
    """Repair object broken and pushed"""
    foll = follower()
    for (remote, name, sha1) in git.remote_generator():
        foll.follow_commit(sha1)
    while foll.get_broken():
        remote_repair_broken_list(git, foll.get_broken())
        foll.restart_on_broken()
    return foll

def repair_remote_branch(git):
    """Repair broken remote branch reference"""
    broken = []
    for (remote, name, sha1) in git.remote_generator():
        if sha1 is None:
           broken.append((remote, name)) 
    if broken:
        print("FETCH")
        launch_command("git fetch ".format(" ".join([remote for (remote, name) in broken])))

    

def basic_repair(git):
    """After this : HEAD is valid, heads too
       No change on index !"""
    #TODO: check that repo index is clean ?
    head = read_all(git["HEAD"]).strip()
    if "refs/heads/" not in head:
        print("head not in a local branch : rewrite to master")
        open(head).write("ref: refs/heads/master")
    cur_branch = read_all(git["HEAD"]).strip()
    cur_branch = cur_branch[1 + cur_branch.rindex("/"):]
    print("Working on branch : {0}".format(cur_branch))
    match = is_sha1(read_all(git["refs"]["heads"][cur_branch]))
    if not match:
        print("broken branch !")
        print ("trying to find a commit to attachement")
        r_branch = find_clean_base_commit(git, "origin", cur_branch)

        if not r_branch:
            print("no remote branch to recover locals !")
            print("abort !")
            print("Pushing is so important...")
            exit(1)
        print("Soft reset into origin{0}".format(r_branch))
        launch_command("git reset --soft origin/{0}".format(r_branch))
    #Commit change into new branch !
    stdout, stderr = launch_command("git commit -a -m 'Commit for FIX by script'")
    if stderr:
        print("Error during commit !")
        print(stderr)
        print("continue ?")
        choice = raw_input(">> ")
        if not ("y" in choice or "oui" in choice):
            exit(1)


def find_clean_base_commit(git, remote, current_branch):
    """Find a commit assured safe (a remote branch)"""
    if "/" in current_branch:
        print("Non canonocal branch name : strip")
        current_branch = current_branch[1 + current_branch.rindex("/"):]
    if current_branch in git["refs"]["remotes"][remote]:
        return current_branch
    if len(git["refs"]["remotes"]["origin"]) == 0:
        return None
    #return ANY other remote branch
    return list(git["refs"]["remotes"]["origin"].keys())[0]
    

def local_repair(git, foll=None):
    basic_repair(git) # HEAD + locals are pointing on valid stuff !
    stdout, stderr = launch_command("git status --porcelain")
    if stdout or stderr:
        print("No clean depot : Fix it please")
        print(stdout)
        print(stderr)
        exit(1)

   #Here everythin should be fine ! 
    if not foll:
        foll = _commit(git)
        if foll.get_broken():
            print("REMOTE NOT REPAIRED : ???")
            exit(1)
    for name, branch in git["refs"]["heads"].items():
        foll.follow_commit(read_all(branch))
        if foll.get_broken():
            r_branch = find_clean_base_commit(git, "origin", name)
            if not r_branch:
                print("no remote branch to recover locals !")
                print("abort !")
                print("Pushing is so important...")
                exit(1)
            launch_command("git checkout {0}".format(branch))
            launch_command("git reset --soft origin/{0}".format(r_branch))
            launch_command("git commit -a -m 'fix branch {0}'".format(branch))




        
    
def repair(path, repair_all=False):
    """ try to repair the git !"""
    check_root_git(path)
    git = generate_complete_git_arbo(path)
    repair_remote_branch(git)
    foll = remote_repair(git)
    if (repair_all):
        local_repair(git ,foll)
    return git


def check(path):
    """This function just check !"""
    check_root_git(path)
    git = generate_complete_git_arbo(path)
    foll = follower()
    #Explore pushed commit
    print("== Recoverable objects ==")
    for (remote, name, sha1) in git.remote_generator():
        foll.follow_commit(sha1)
        if not foll.get_broken():
            continue
        print("Found broken objects for <{0}/{1}>".format(remote, name))
        for (t, value) in foll.broken.items():
            print ("    <{0}> : {1}".format(t, list(value)))
        foll.reset_broken()
    #Explore non pushed commit
    print("== NON Recoverable objects !==")
    for (name, sha1) in git.branch_generator():
        foll.follow_commit(sha1)
        if not foll.get_broken():
            continue
        print("Found broken objects for <{0}>".format(name))
        for (t, value) in foll.broken.items():
            print ("    <{0}> : {1}".format(t, list(value)))
        foll.reset_broken()
