import subprocess
import re
git_subdir = ".git"
git_cat_file = "git cat-file -p "

def launch_command(command):
    return subprocess.Popen(command, stdout = subprocess.PIPE, stderr= subprocess.PIPE, shell=True).communicate()


class follow_decorate(object):
    def __init__(self, type):
        self.type = type

    #Actual decorator function
    def __call__(self, f):
        self.func = f
        def decorate(other_self, sha1):
            stdout, stderr = launch_command(git_cat_file + sha1)
            if sha1 in other_self.followed:
                return
            if stderr:
                print ("Broken {1}: {0}".format(sha1, self.type))
                other_self.broken[self.type].add(sha1.strip())
                return
            #Followed if not broken
            other_self.followed.add(sha1)
            self.func(other_self, stdout.decode())
        return decorate

class follower(object):
    def __init__(self):
        self.broken = {t : set() for t in ["commit", "tree", "blob"]}
        self.followed = set()

    @follow_decorate("commit")
    def follow_commit(self, stdout):  
        tree = re.match("tree [0-9a-z]{40}", stdout).group().split()[1]
        self.follow_tree(tree)
        for parent in re.findall("parent [0-9a-z]{40}", stdout):
            self.follow_commit(parent.split()[1])
       
    @follow_decorate("tree")
    def follow_tree(self, stdout): 
        for line in stdout.split('\n')[:-1]:
            a, gtype, gsha1, name = line.split()
            if gtype == "tree":
                self.follow_tree(gsha1)
            else:
                self.follow_blob(gsha1)
       
    @follow_decorate("blob")
    def follow_blob(self, stdout): 
        return

    def restart_on_broken(self):
        broken = self.broken
        self.reset_broken()
        for t in broken:
            follow = getattr(self, "follow_" + t)
            for sha1 in broken[t]:
                follow(sha1)

    def get_broken(self):
        l = []
        [l.extend(brok) for brok in self.broken.values()]
        return l

    def reset_broken(self):
        self.broken = {t : set() for t in ["commit", "tree", "blob"]}
