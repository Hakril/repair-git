import argparse
from explore import repair, check, GitRepo


parser = argparse.ArgumentParser(description="Git repair option")
parser.add_argument("-r", dest="repair", action="store_const", help="Repair using remote", const=True, default=False)
parser.add_argument("-a", dest="repair_all", action="store_const", help="Try to repair everything the best we can (hazardous)",const=True, default=False)
parser.add_argument("path", nargs="?", action="store",type=str, default=".")

args = parser.parse_args()

if args.repair:
    repair(args.path, args.repair_all)
else:
    check(args.path)
